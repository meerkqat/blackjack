from Tkinter import*
from random import*
from time import sleep

okno = Tk()
okno.title("Blackjack")

okno.geometry('{}x{}+{}+{}'.format(907, 400, 350, 100))

slike = []
deck = []
mc = []
dc = []
mc1 = []
used_c = []
used_s = []
nods = 2
noddy = 2

BANK_MONEY_CONST = 2000.0
PLAYER_MONEY_CONST = 100.0

bank_money = BANK_MONEY_CONST

bank_v = StringVar(okno, value = "Bank: "+str(float(bank_money)))
money_v = StringVar(okno, value = "100.0")
bc=StringVar(okno,value="Card back")
result_v = StringVar(okno, value="Last game: ")
bet_v = StringVar(okno, value="Last bet: ")
gain_v = StringVar(okno, value="Last gain: ")
cpub_v = StringVar(okno, value="Computer busted?")

print "Blackjack v. 4.3 by Jurij Slabanja\n"

def about():
	about_top = Toplevel()
	about_top.title("About...")
	
	photo = PhotoImage(file="pics/phoenix.gif")
	pic = Label(about_top, image = photo)
	pic.photo  = photo
	pic.grid(column = 0, row = 0, pady = 8)

	msg = Label(about_top, text="Blackjack v. 4.3\nAuthor: Jurij Slabanja\nJuly 2014")
	msg.grid(column = 0, row = 1, sticky=W, padx = 10)

	button = Button(about_top, text="OK", width = 10, command=about_top.destroy)
	button.grid(column = 0, row = 2, pady = 8)

def bet_f():
	global bet, cash, mc, tm
	bet = float(bet_vnos.get()),0.0
	if bet[0] <= cash and bet[0] > 0:
		hit_bttn.config(state = NORMAL)
		stand_bttn.config(state = NORMAL)
		double_bttn.config(state = NORMAL)
		bet_vnos.config(state = DISABLED)
		bet_bttn.config(state = DISABLED)
		surrender_bttn.config(state = NORMAL)
		if mc[0][-1] == mc[-1][-1]:
			split_bttn.config(state = NORMAL)
			
		povrsina.delete(back1)
		povrsina.delete(back2)
		povrsina.delete(back3)
		povrsina.delete(tm)
		
		if blackjack(mc, True) == True:
			tm = povrsina.create_text(50,240,text = "Blackjack", fill="#FFFFFF")
			izid()
		else:
			vsota_ = suma(mc)
			tm = povrsina.create_text(50,240,text = "Value: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
			cash = cash-bet[0]
			money_v.set(str(cash))

def blackjack(d, id_jst):
	global possible
	bj = False
	tens = ["T","J","Q","K"]
	if possible == True or id_jst == False:
		for e in tens :
			if d[0][-1] == "A" and d[-1][-1] == e and len(d) == 2 or d[0][-1] == e and d[-1][-1] == "A" and len(d) == 2:
				bj = True
				break
	return bj

def calc_xcord(d,offset=0):
    l = len(d)
    xcord = 50 + 30*l + offset
    return xcord

def check(moja_v):
    if moja_v > 21:
        return True
    else:
        return False
	
def dodaj(slike, deck):
	global used_c, used_s
	slika = slike.pop(0)
	card = deck.pop(0)
	used_s.append(slika)
	used_c.append(card)
	return slika, card

def double():
	global bet, cash, mc, tm, vsota_
	if cash>= bet[0]:
		double_bttn.config(state = DISABLED)
		
		cash = cash-bet[0]
		bet = (bet[0]*2,0.0)
		money_v.set(str(cash))
		
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc),180,image=slika)
		mc.append(card)
		vsota_ = suma(mc)
		povrsina.delete(tm)
		tm = povrsina.create_text(50,240,text = "Value: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")

		if check(vsota_[-1]) == True:
			izid()
		else:
			stand()

def hit():
	global active, id_, mc, mc1, tm, tm1, vsota_, vsota_1
	double_bttn.config(state = DISABLED)
	split_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	if active == 1:
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc),180,image=slika)
		mc.append(card)
		vsota_ = suma(mc)
		povrsina.delete(tm)
		tm = povrsina.create_text(50,240,text = "Value: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
	elif active == 2:
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc1,420),180,image=slika)
		mc1.append(card)
		vsota_1 = suma(mc1)
		povrsina.delete(tm1)
		tm1 = povrsina.create_text(470,240,text = "Value: " + vsota_1[0]+str(vsota_1[-1]), fill="#FFFFFF")

	if check(vsota_[-1]) == True:
		if id_ == 2 and len(mc1) == 1:
			stand()
		elif id_ == 1:
			izid()
	if check(vsota_[-1]) == False and check(vsota_1[-1]) == True:
		stand()
	elif check(vsota_[-1]) == True and check(vsota_1[-1]) == True:
		izid()

def izid():
	global back, bet, cash, dc, id_, mc, mc1, td, bank_money, noddy, bank_v
	hit_bttn.config(state = DISABLED)
	stand_bttn.config(state = DISABLED)
	double_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	split_bttn.config(state = DISABLED)

	bet_array = []
	konc_array = []
	for i in range(id_):
		tren_bet = bet[i]
		dv = suma(dc)[-1]
		if i == 0:
			mv = suma(mc)[-1]
			bet_str = str(bet[0])
		elif i == 1:
			mv = suma(mc1)[-1]
			bet_str = str(bet[0])+", "+str(bet[1])
			
		bet_v.set("Last bet: " +bet_str)
		
		if check(mv) == True:
			konc = "Busted"
			bet_array.append("-"+str(bet[i]))
			
		elif blackjack(mc, True) == True and blackjack(dc, False) == False:
			konc = "Blackjack"
			cash = cash+tren_bet+(tren_bet*1.5)
			bet_array.append(str(bet[i]+bet[i]*1.5))

		elif dv < mv or check(dv) == True and mv <= 21:
			konc = "Win"
			cash = cash+tren_bet*2
			bet_array.append(str(bet[i]*2))
					
		elif dv == mv:
			if blackjack(mc, True) == True and blackjack(dc, False) == True:
				povrsina.delete(back)
				povrsina.delete(td)
				td = povrsina.create_text(50,120,text = "Blackjack", fill="#FFFFFF")
			konc = "Push"
			cash = cash+tren_bet
			bet_array.append(str(bet[i]))
		
		elif dv > mv and check(dv) == False:
			konc = "Lost"
			bet_array.append("-"+str(bet[i]))
		
		konc_array.append(konc)
	
	be = 0.0
	for e in bet_array:
		be = be+float(e)
	
	gain_v.set("Last gain: " + str(be))

	bank_money -= be
	bank_v.set("Bank: "+str(float(bank_money)))
	if bank_money <= 0:
		endgame_top = Toplevel()
		endgame_top.title("You won!")
		endgame_top.geometry('{}x{}+{}+{}'.format(200, 140, 700, 250))
		msg = Message(endgame_top, text="\nCongratulations!\n\nThe bank is out of money.\n")
		msg.pack()

		button = Button(endgame_top, text="New Game", command=lambda: new_game_plus_destroy(noddy, endgame_top))
		button.pack()

	if cash <= 0:
		endgame_top = Toplevel()
		endgame_top.title("You lost.")
		endgame_top.geometry('{}x{}+{}+{}'.format(200, 90, 700, 250))
		msg = Message(endgame_top, text="Better luck next time.\n")
		msg.pack()

		button = Button(endgame_top, text="New Game", command=lambda: new_game_plus_destroy(noddy, endgame_top))
		button.pack()

	result_v.set("Last game: " + ", ".join(konc_array))
	if dv > 21:
		cpub = "Yes"
	else:
		cpub = "No"
	cpub_v.set("Computer busted? " + cpub)
	
	money_v.set(str(cash))

def mkdeck(nod):
	global deck, slike
	d = []
	num = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
	suit = ["H","D","S","C"]
	for n in range(nod):
		for e in num:
			for f in suit:
				deck.append(f+e)

	for i in range(0,len(deck)):
		slike.append(PhotoImage(file="pics/"+deck[i]+".gif"))
	card_back = PhotoImage(file="pics/Deck2.gif")
	
	for i in range(2):
		deck, slike = same_random2(deck, slike)
	
def new_game(nod = nods):
	global cash, deck, slike, used_c, used_s, noddy, bank_money
	noddy = nod
	cash = PLAYER_MONEY_CONST
	bank_money = BANK_MONEY_CONST
	money_v.set(str(cash))
	deck = rm_array(deck)
	slike = rm_array(slike)
	used_c = rm_array(used_c)
	used_s = rm_array(used_s)
	mkdeck(nod)
	bank_v.set("Bank: "+str(float(bank_money)))
	new_round()
	
def new_game_plus_destroy(noddy, endgame_top):
	endgame_top.destroy()
	new_game(noddy)

def new_round():
	global active, back, back1, back2, back3, bet, card_back, dc, deck, id_, mc, mc1, possible, slike, td, tm, used_c, used_s, vsota, vsota_, vsota_1
	hit_bttn.config(state = DISABLED)
	stand_bttn.config(state = DISABLED)
	double_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	split_bttn.config(state = DISABLED)
	
	bet = 0.0,0.0
	vsota = "",0
	vsota_ = "",0
	vsota_1 = "",0
	id_ = 1
	active = 1
	possible = True
	pcent = 75.0

	bet_vnos.config(state = NORMAL)
	bet_bttn.config(state = NORMAL)

	povrsina.delete(ALL)
	mc = rm_array(mc)
	dc = rm_array(dc)
	mc1 = rm_array(mc1)

	if float(len(used_c))*float(100-pcent) >= float(len(deck))*pcent:
		for c in range(0,len(used_c)):
			card = used_c.pop(0)
			deck.append(card)
		for s in range(0,len(used_s)):
			slika = used_s.pop(0)
			slike.append(slika)

		deck, slike = same_random2(deck, slike)
		print "Shuffled the deck!"

	back = bc.get()
	if back == "Hand":
		card_back = PhotoImage(file="pics/Deck1.gif")
	elif back == "Rose":
		card_back = PhotoImage(file="pics/Deck2.gif")
	elif back == "Vine":
		card_back = PhotoImage(file="pics/Deck3.gif")
	elif back == "Blue":
		card_back = PhotoImage(file="pics/Deck4.gif")
	elif back == "Red":
		card_back = PhotoImage(file="pics/Deck5.gif")
	else:
		card_back = PhotoImage(file="pics/Deck2.gif")

	for i in range (2):
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc),180,image=slika)
		mc.append(card)
		
		back = povrsina.create_image(50 ,60,image=card_back)

		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(dc),60,image=slika)
		dc.append(card)

	back1 = povrsina.create_image(80 ,60,image=card_back)
	back2 = povrsina.create_image(50 ,180,image=card_back)
	back3 = povrsina.create_image(80 ,180,image=card_back)

	td = povrsina.create_text(50,120,text = "Value: X", fill="#FFFFFF")
	tm = povrsina.create_text(50,240,text = "Value: X", fill="#FFFFFF")

def rm_array(a):
	del a
	a = []
	return a
	
def rules ():
	f = open("pics/rulebook.txt", "r")
	for e in f:
		print e
	f.close()
	
def same_random2(my_array1, my_array2):

	for i in range(len(my_array1)):
		x_pop = randint(0, len(my_array1)-1)
		x_insert = randint(0, len(my_array1)-2)
		f = my_array1.pop(i)
		g = my_array2.pop(i)
		my_array1.insert(x_insert, f)
		my_array2.insert(x_insert, g)
	
	return my_array1, my_array2

def split():
	global back, bet, cash, id_, mc, mc1, possible, td, tm, tm1, used_s, vsota_, vsota_1
	split_bttn.config(state = DISABLED)
	double_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	povrsina.delete(ALL)
	possible = False
	bet = bet[0],bet[0]

	cash = cash-bet[0]
	money_v.set(str(cash))
	td = povrsina.create_text(50,120,text = "Value: X", fill="#FFFFFF")
	povrsina.create_image(50,60,image=used_s[-3])
	back = povrsina.create_image(50 ,60,image=card_back)
	povrsina.create_image(80,60,image=used_s[-1])
	povrsina.create_image(50,180,image=used_s[-4])
	povrsina.create_image(470,180,image=used_s[-2])
	mc1 = [mc.pop(-1)]
	
	slika,card = dodaj(slike,deck)
	povrsina.create_image(80,180,image=slika)
	mc.append(card)
	
	vsota_ = suma(mc)
	tm = povrsina.create_text(50,240,text = "Value: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
	vsota_1 = suma(mc1)
	tm1 = povrsina.create_text(470,240,text = "Value: " + vsota_1[0]+str(vsota_1[-1]), fill="#FFFFFF")
	id_ = 2
	
def stand():
	global active, dc, deck, id_, mc, mc1, slike, td, tm1, vsota, vsota_1
	if id_ == 1 or id_ ==2 and active == 2:
		hit_bttn.config(state = DISABLED)
		stand_bttn.config(state = DISABLED)
		double_bttn.config(state = DISABLED)
		surrender_bttn.config(state = DISABLED)
		split_bttn.config(state = DISABLED)

		vsota = suma(dc)
		povrsina.delete(back)
		if vsota[-1] < 17:
			while vsota[-1] < 17:
				slika,card = dodaj(slike,deck)
				povrsina.create_image(calc_xcord(dc),60,image=slika)
				dc.append(card)
				vsota = suma(dc)

		povrsina.delete(td)
		if blackjack(dc, False) == True:
			td = povrsina.create_text(50,120,text = "Blackjack", fill="#FFFFFF")
		else:
			td = povrsina.create_text(50,120,text = "Value: " + vsota[0]+str(vsota[-1]), fill="#FFFFFF")
		izid()
	
	elif id_ == 2 and len(mc1) == 1:
		active = 2
		slika,card = dodaj(slike,deck)
		povrsina.create_image(500,180,image=slika)
		mc1.append(card)
		vsota_1 = suma(mc1)
		povrsina.delete(tm1)
		tm1 = povrsina.create_text(470,240,text = "Value: " + vsota_1[0]+str(vsota_1[-1]), fill="#FFFFFF")

def suma(d):
	a = 0
	b = "",0
	soft = True
	for e in d:
		a = e[-1]
		if a == "J" or a == "Q" or a == "K" or a == "T":
			a = 10
		elif a == "A":
			a = 11
		else:
			a = int(a)
		b ="",a+b[-1]

	for f in d:
		if b[-1] > 21 and f[-1] == "A":
			b ="", b[-1]-10
			soft = False
	for g in d:
		if g[-1] == "A":
			if soft == True:
				b = "Soft ", b[-1]
	return b

def surrender():
	global bet, cash, bank_money
	cash = cash+bet[0]/2
	bank_money += bet[0]/2
	money_v.set(str(cash))
	bank_v.set("Bank: "+str(float(bank_money)))
	result_v.set("Last game: Surrender")
	bet_v.set("Last bet: " + str(bet[0]))
	gain_v.set("Last gain: " + str(bet[0]/2))
	cpub_v.set("Computer busted? No" )
	new_round()

def stats_disp():
	stats_top = Toplevel()
	stats_top.title("Stats")
	#stats_top.config(width=100)
	stats_top.geometry('{}x{}+{}+{}'.format(200, 130, 100, 100))

	bank = Label(stats_top, textvar = bank_v)
	bank.grid(column=1, row=1, sticky=W)
	dummy = Label(stats_top, text="                                                                                             ")
	dummy.grid(column=1, row=2, sticky=W)
	result = Label(stats_top, textvar = result_v)
	result.grid(column=1, row=3, sticky=W)
	bet_res = Label(stats_top, textvar = bet_v)
	bet_res.grid(column=1, row=4, sticky=W)
	gain = Label(stats_top, textvar = gain_v)
	gain.grid(column=1, row=5, sticky=W)
	cpu_b = Label(stats_top, textvar = cpub_v)
	cpu_b.grid(column=1, row=6, sticky=W)

menu = Menu(okno)
okno.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="New", command=lambda: new_game(noddy))
filemenu.add_command(label="Stats", command=lambda: stats_disp())
filemenu.add_separator()
filemenu.add_command(label="Exit", command=okno.quit)

deckmenu = Menu(menu)
menu.add_cascade(label="No. of decks", menu=deckmenu)
deckmenu.add_radiobutton(label="1 Deck", command=lambda: new_game(1))
deckmenu.add_radiobutton(label="2 Decks", command=lambda: new_game(2))
deckmenu.add_radiobutton(label="4 Decks", command=lambda: new_game(4))
deckmenu.add_radiobutton(label="6 Decks", command=lambda: new_game(6))
deckmenu.add_radiobutton(label="8 Decks", command=lambda: new_game(8))

helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Rules", command=rules)
helpmenu.add_command(label="About...", command=about)

	
povrsina = Canvas(okno, width=900, height=250, bg="#006500")
povrsina.grid(row=0, column=0,columnspan= 10)

money = Label(okno, text="Money: ")
money.grid(column=0, row=1, sticky=E, padx=5)
bet_txt = Label(okno, text = "Bet")
bet_txt.grid(column=0, row=2)

money_got = Label(okno, textvar = money_v)
money_got.grid(column=1, row=1, sticky=W)
bet_vnos = Entry(okno, width = 10, state = NORMAL)
bet_vnos.grid(column=1, row=2)
bet_bttn = Button(okno, text = "Bet", width = 10, command = bet_f, state = NORMAL)
bet_bttn.grid(column=1, row=3)

hit_bttn = Button(okno, text = "Hit", width = 20, command = hit, state = DISABLED)
hit_bttn.grid(column=2, row=2)
stand_bttn = Button(okno, text = "Stand", width = 20, command = stand, state = DISABLED)
stand_bttn.grid(column=2, row=3)
double_bttn = Button(okno, text = "Double down", width = 20, command = double, state = DISABLED)
double_bttn.grid(column=2, row=4)
split_bttn = Button(okno, text = "Split", width = 20, command = split, state = DISABLED)
split_bttn.grid(column=2, row=5)

surrender_bttn = Button(okno, text = "Surrender", width = 20, command = surrender, state = DISABLED)
surrender_bttn.grid(column=3, row=2)
nr_bttn = Button(okno, text = "New round", width = 20, command = new_round)
nr_bttn.grid(column=3, row=3)
backc = OptionMenu(okno,bc,"Hand","Rose","Vine","Blue","Red") 
backc.grid(column=3, row=4, sticky=NW)

spacer=Label(okno)
spacer.grid(column=4, padx=190)

stats_disp()

new_game()

okno.mainloop()

# -*- coding: cp1250 -*-
from random import*
from time import sleep

# limit - na to vrednost bo ra�unalnik �e tolkel, �e ima� slab�e karte kot dealer
limit = 17
cash = 50
konc = 0

def deck():
    d = []
    deck = []
    num = ["2","3","4","5","6","7","8","9","10","Jack","Queen","King","Ace"]
    suit = ["Hearts","Diamonds","Spades","Clubs"]

    for e in num:
        for f in suit:
            d.append([e,f])
    for e in d:
        a = e.pop(0)
        b = e.pop(-1)
        c = a + " of " + b
        c = str(c)
        deck.append(c)
    return deck

#vrne True �e si �el �ez 21
def check(moja_v):
    if moja_v > 21:
        return True
    else:
        return False
        
def izberi(deck):
    l = len(deck)
    x = randint(0,l-1)
    izbira = deck.pop(x)
    return izbira

def suma(d):
    a = 0
    b = 0
    for e in d:
        a = e[0]
        if a == "J" or a == "Q" or a == "K" or a == "A" or a == "1":
            a = 10
        else:
            a = int(a)
        b = a+b
        for f in d:
            if b > 21 and f[0] == "A":
                b = b-9
    vsota = b    
    return vsota

def print1(mc,dc):
    l = len(dc[0])
    dc = ", ".join(dc)
    dc = dc[l+2:]
    print "Dealer's cards: ", "X, ", dc
    vsota = suma(mc)
    mc = ", ".join(mc)
    print "My cards: ", mc
    print "My cards' value: ", vsota

def print2(c):
    if c == dc:
        a = "Dealer's"
    elif c == mc:
        a = "My"
    vsota = suma(c)
    c = ", ".join(c)
    print a, "cards:", c
    print a, "cards' value:", vsota
    return vsota


def end(v1,v2):
    if v1 == v2:
        end = 1
    elif v1 > v2 and check(v1) == False:
        end = 2
    elif v1 < v2 or check(v1) == True:
        end = 3
    return end

def end_(konc):
    if konc == 1:
        print "Tie"
    elif konc == 2:
        print "Lose"
    elif konc == 3:
        print "Win"


deck = deck()
while cash > 0:
    mc = []
    dc = []

    print "Cash: ", cash
    bet = input("Bet: ")
    while bet > cash:
        bet = input("Bet: ")
    
    for i in range (2):
        mc.append(izberi(deck))
        dc.append(izberi(deck))
        i = i+1
    print1(mc,dc)

    vsota_ = suma(mc)
    while check(vsota_) == False:
        a = raw_input("Do? ")
        if a == "hit" or a == "h":
            mc.append(izberi(deck))
            print1(mc,dc)
            vsota_ = suma(mc)
            check(vsota_)

        if a == "stop" or a == "s":
            vsota_ = suma(mc)
            vsota = suma(dc)
            if vsota <= limit or vsota < vsota_:
                while vsota <= limit or vsota < vsota_:
                    dc.append(izberi(deck))
                    vsota = print2(dc)
                    vsota_ = print2(mc)
                    sleep(1.5)
                konc = end(vsota, vsota_)
                xyz = end_(konc)
                break
            else:
                vsota = print2(dc)
                vsota_ = print2(mc)
                end = end(vsota, vsota_)
                xyz = end_(konc)
                break
    
    if check(vsota_) == True:
        print "Busted"

    if konc == 3:
        cash = cash+2*bet 
    elif konc == 2 or check(vsota_) == True:
        cash = cash-bet

    for e in mc:
        mc = mc.remove(e)
        break
    
    for e in dc:
        dc = dc.remove(e)
        break
    
    vsota = 0
    vsota_ = 0
    print "\n"

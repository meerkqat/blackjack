from Tkinter import*
from random import*

okno = Tk()
okno.title("Blackjack")

slike = []
deck = []
mc = []
dc = []
mc1 = []
used_c = []
used_s = []
# number of decks (st. kupckov na zacetku)
nods = 2

# zacetne spemenlivke od statistike spodaj desno
money_v = StringVar(okno, value = "100.0")
bc=StringVar(okno,value="Hrbet kart")
result_v = StringVar(okno, value="Zadnja igra: ")
bet_v = StringVar(okno, value="Zadnja stava: ")
gain_v = StringVar(okno, value="Zadnji dobitek: ")
cpub_v = StringVar(okno, value="Racunlanik busted?")

print "Blackjack v. 4.2 by Jurij Slabanja\n"

# narise okno about
def about():
	about_top = Toplevel()
	about_top.title("About...")
	# slika
	photo = PhotoImage(file="slike/phoenix.gif")
	pic = Label(about_top, image = photo)
	pic.photo  = photo
	pic.grid(column = 0, row = 0, pady = 8)
	# sporocilo
	msg = Label(about_top, text="Blackjack v. 4.2\nIzdelal Jurij Slabanja\nOktober 2010")
	msg.grid(column = 0, row = 1, sticky=W, padx = 10)
	# gumb
	button = Button(about_top, text="OK", width = 10, command=about_top.destroy)
	button.grid(column = 0, row = 2, pady = 8)

"""
## Funkcija, ki jo poklice gumb Stavi
		Potrdi stavo, ki si jo vpisal
"""
def bet_f():
	global bet, cash, mc, tm
	#dobi stavo, ki si jo vpisal v okno in preveri ce je veljavna
	# (vecja od 0 in manjsa ali enaka kolicini denarja, ki ga imas)
	bet = float(bet_vnos.get()),0.0
	if bet[0] <= cash and bet[0] > 0:
		# spremeni stanje gumbom
		hit_bttn.config(state = NORMAL)
		stand_bttn.config(state = NORMAL)
		double_bttn.config(state = NORMAL)
		bet_vnos.config(state = DISABLED)
		bet_bttn.config(state = DISABLED)
		surrender_bttn.config(state = NORMAL)
		# ce imata obe zacetni karti enako vrednost vklopi tudi gumb split
		if mc[0][-1] == mc[-1][-1]:
			split_bttn.config(state = NORMAL)
			
		# pocisti hrbte (odkrij karte); razen delivceve prve
		povrsina.delete(back1)
		povrsina.delete(back2)
		povrsina.delete(back3)
		povrsina.delete(tm)
		
		# preveri, ce sem dobil blackjack (pojdi na izid), 
		# sicer napisi vsoto in ponovno napisi koliko denarja imam
		if blackjack(mc, True) == True:
			tm = povrsina.create_text(50,240,text = "Blackjack", fill="#FFFFFF")
			izid()
		else:
			vsota_ = suma(mc)
			tm = povrsina.create_text(50,240,text = "Vsota: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
			cash = cash-bet[0]
			money_v.set(str(cash))

"""
Funkcija, ki pogleda, ce je vrednost dveh kart 21 (blackjack)
@param
	d			=	array; deck oz. katero roko naj gleda
	id_jst	=	bool; identifier - True = moja roka, False je delivceva roka
@return
	bj			=	bool; True = blackjack, False != blackjack
"""
def blackjack(d, id_jst):
	global possible
	bj = False
	tens = ["T","J","Q","K"]
	# possible se uporablja pri razcepu kart(ko to naredis ne mores vec dobiti BJ; possible = False)
	# id_jst je pa pomemben, ker tudi ce razcepis karte ima delivec se vedno lahko BJ
	if possible == True or id_jst == False:
		# pojdi po vseh desetkah in ce je prva karta as, druga desetka (ali obratno)
		# in imas samo 2 karti v roki potem to je BJ
		for e in tens :
			if d[0][-1] == "A" and d[-1][-1] == e and len(d) == 2 or d[0][-1] == e and d[-1][-1] == "A" and len(d) == 2:
				bj = True
				break
	return bj

"""
Funkcija, ki izracuna x koordinato na podlagi stevila kart v roki(da zna narisat)
@param
	d			=	array; deck oz. katero roko naj gleda
	offset	=	int; odmikod originalne pozicije (ce razcepis karte, da dobis dve roki); default = 0
@return
	xcord		=	int; x koordinata
"""
def calc_xcord(d,offset=0):
    l = len(d)
    xcord = 50 + 30*l + offset
    return xcord

"""
Funkcija, ki iz kupcka pobere karto in vrne vrednost karte ter njeno sliko
@param
	slike	=	array; vseh (preostalih) slik kart
	deck	=	array; vseh (preostalih) imen oz. vrednosti kart
@return
	slika	=	PhotoImage; slika izbrane karte
	card	=	str; ime uzbrane karte
"""		
def dodaj(slike, deck):
	global used_c, used_s
	# iz kupcka vzemi eno karto
	slika = slike.pop(0)
	card = deck.pop(0)
	# pripni jo na kupcek porabljenih kart
	used_s.append(slika)
	used_c.append(card)
	#in jo vrni
	return slika, card

"""
## Funkcija, ki jo poklice gumb Dvojna
		Z dvojno igralec podvoji stavo in prejme SAMO SE ENO karto
"""
def double():
	global bet, cash, mc, tm, vsota_
	# preveri ce sploh imas dovolj denarja za dvojno
	if cash>= bet[0]:
		double_bttn.config(state = DISABLED)
		
		#popravki glede stave in denarja
		cash = cash-bet[0]
		bet = (bet[0]*2,0.0)
		money_v.set(str(cash))
		
		#razdeli karto in jo narisi
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc),180,image=slika)
		mc.append(card)
		vsota_ = suma(mc)
		povrsina.delete(tm)
		tm = povrsina.create_text(50,240,text = "Vsota: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
		
		# preveri ce si sel cez 21
		if vsota_[-1] > 21:
			izid()
		else:
			stand()
	
"""
## Funkcija, ki jo poklice gumb Tolci
		S tolcenjem prejmes se eno karto
		
// Pomembnejsi globalni spremenljivki sta:
		active	=	int; vrednost = 1 ali 2; ce si razcepil roko, pove katero 
						mora trenutno gledat - levo ali desno
		id_		=	int; vrednost = 1 ali 2; ce si razcepil roko zavzema vrednost 2, sicer 1
						(koliko rok imas v igri)
"""
def hit():
	global active, id_, mc, mc1, tm, tm1, vsota_, vsota_1
	# ko enkrat tolces ne mores vec razcepit kart, podvojiti stave ali se predati
	double_bttn.config(state = DISABLED)
	split_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	# leva roka - razdeli novo karto
	if active == 1:
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc),180,image=slika)
		mc.append(card)
		vsota_ = suma(mc)
		povrsina.delete(tm)
		tm = povrsina.create_text(50,240,text = "Vsota: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
	# desna roka - razdeli novo karto
	elif active == 2:
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc1,420),180,image=slika)
		mc1.append(card)
		vsota_1 = suma(mc1)
		povrsina.delete(tm1)
		tm1 = povrsina.create_text(470,240,text = "Vsota: " + vsota_1[0]+str(vsota_1[-1]), fill="#FFFFFF")
	
	# ->preveri vsoto roke
	# ce si sel cez 21 s prvo roko
	if vsota_[-1] > 21:
		# in imas 2 roki v igri poklici stand(), ki bo "aktiviral" naslednjo roko
		if id_ == 2 and len(mc1) == 1:
			stand()
		# in imas samo eno roko v igri poklici izid()
		elif id_ == 1:
			izid()
	# ce si sel cez 21 z drugo roko
	# in s prvo nisi, poklici stand()
	if vsota_[-1] <= 21 and vsota_1[-1] > 21:
		stand()
	# sicer poklici izid()
	elif vsota_[-1] > 21 and vsota_1[-1] > 21:
		izid()
	
"""
Funkcija, ki pogleda kdo je zmagal, izpise statistiko
in ti izplaca ustrezno kolicino denarja
"""
def izid():
	global back, bet, cash, dc, id_, mc, mc1, td
	# spremeni stanje gumbom
	hit_bttn.config(state = DISABLED)
	stand_bttn.config(state = DISABLED)
	double_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	split_bttn.config(state = DISABLED)

	"""----------------------------------------------------------------------------------"""
	
	bet_array = []
	konc_array = []
	# id_ je 1 ali 2, odvisno od stevila rok ki si jih imel v igri
	# (zacnes z eno, ce razcepis dobis dve)
	# bet je tuple(z dvema vrednostima):
	# 		1. originalna stava
	#		2. stava od razcepa roke
	for i in range(id_):
		tren_bet = bet[i]
		dv = suma(dc)[-1]
		
		# tole pride v postev za zapis statistike stave
		# poleg tega se izracuna tudi mv(moja vsota)
		# oz. vsota roke, ki jo trenutno gledas
		if i == 0:
			mv = suma(mc)[-1]
			bet_str = str(bet[0])
		elif i == 1:
			mv = suma(mc1)[-1]
			bet_str = str(bet[0])+", "+str(bet[1])
		# zapis statistike stave
		bet_v.set("Last bet: " +bet_str)
		
		# ce je mv vecja od 21
		if mv > 21:
			konc = "Busted"
			bet_array.append("-"+str(bet[i]))
			
		# ce imas BJ, ampak ga delivec nima
		elif blackjack(mc, True) == True and blackjack(dc, False) == False:
			konc = "Blackjack"
			# za BJ dobis malo vec denarja nazaj - ponavadi se steje 3:2 za BJ
			cash = cash+tren_bet+(tren_bet*1.5)
			bet_array.append(str(bet[i]+bet[i]*1.5))
			
		# ce ima delivec manjso vrednost kart kot ti ALI
		# ce je delivec sel cez 21, ampak ti nisi 
		elif dv < mv or dv > 21 and mv <= 21:
			konc = "Zmaga"
			cash = cash+tren_bet*2
			bet_array.append(str(bet[i]*2))
		
		# ce imata ti in delivec isto vrednost kart
		elif dv == mv:
			# ce sem jaz dobil BJ mora delivec tudi pokazati, da ga ima
			# ce ga pa jaz nimam, ga ima pa delivec se pa to obravnava v stand()
			if blackjack(mc, True) == True and blackjack(dc, False) == True:
				povrsina.delete(back)
				povrsina.delete(td)
				td = povrsina.create_text(50,120,text = "Blackjack", fill="#FFFFFF")
			konc = "Push"
			cash = cash+tren_bet
			bet_array.append(str(bet[i]))
		
		# ce ima delivec vecjo vrednost kart kot ti in ni sel cez 21
		elif dv > mv and dv <=21:
			konc = "Izgubil"
			bet_array.append("-"+str(bet[i]))
		
		# ker imas lahko dva razlicna izida(ce si razcepil roko)
		# se le-ti filajo v konc_array, da jih lahko potem zapises v statistiko
		konc_array.append(konc)
		
	"""----------------------------------------------------------------------------------"""
	
	# sestej stave( ce imas dve, sicer je druga tako ali tako 0)
	be = 0.0
	for e in bet_array:
		be = be+float(e)
	
	#zapis statistike
	gain_v.set("Zadnji dobitek: " + str(be))
	result_v.set("Zadnja igra: " + ", ".join(konc_array))
	# preveri ce je delivec sel cez 21 in zapisi statistiko
	if dv > 21:
		cpub = "Da"
	else:
		cpub = "Ne"
	cpub_v.set("Racunalnik busted? " + cpub)
	
	#napisi koliko denarja imam
	money_v.set(str(cash))

"""
Funkcija, ki naredi (zmesan) kupcek kart; 
t.j. array vseh vrednosti kart (deck) in array vseh slik kart (slike)
@param
	nod	=	int; 'number of decks' - t.j. koliko kupckov naj da skupaj
@return
	nicesar ne vrne, ker sta deck in slike globalni spremenljivki
"""
def mkdeck(nod):
	global deck, slike
	d = []
	num = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
	suit = ["H","D","S","C"]
	# s kartezicnim produktom vseh cifer in vseh barv
	#	dobis vse vrednosti kart; to pa ponovi nod-krat
	for n in range(nod):
		for e in num:
			for f in suit:
				deck.append(f+e)
	# vsaki vrednosti priredi ustrezno sliko
	for i in range(0,len(deck)):
		slike.append(PhotoImage(file="slike/"+deck[i]+".gif"))
	card_back = PhotoImage(file="slike/Deck2.gif")
	# zmesaj kupcek (dvakrat)
	for i in range(2):
		deck, slike = same_random2(deck, slike)

"""
Funkcija, ki zacne novo igro (ne novo rundo); ponastavi vse vrednosti
@param
	nod	=	int; 'number of decks' - t.j. koliko kupckov naj da skupaj
				default = nods (se nahaja cisto na zacetku programa)
@return
	nicesar ne vrne, ker so vse spremenljivke globalne
"""
def new_game(nod = nods):
	global cash, deck, slike, used_c, used_s
	# ponastavi denar
	cash = 100.0
	money_v.set(str(cash))
	# ponastavi kupcek
	deck = rm_array(deck)
	slike = rm_array(slike)
	used_c = rm_array(used_c)
	used_s = rm_array(used_s)
	mkdeck(nod)
	# nova runda
	new_round()

"""
## Funkcija, ki jo klice gumb Nov krog
"""	
def new_round():
	global active, back, back1, back2, back3, bet, card_back, dc, deck, id_, mc, mc1, possible, slike, td, tm, used_c, used_s, vsota, vsota_, vsota_1
	# spremeni stanja gumbom
	hit_bttn.config(state = DISABLED)
	stand_bttn.config(state = DISABLED)
	double_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	split_bttn.config(state = DISABLED)
	
	# ponastavi vrednosti stave, 
	# trenutnih vsot, koliko rok imas v igri, 
	# katera roka je tren. aktivna, ali je mozno
	# dobiti BJ
	bet = 0.0,0.0
	vsota = "",0
	vsota_ = "",0
	vsota_1 = "",0
	id_ = 1
	active = 1
	possible = True
	# pcent pove koliko kupcka lahko delivec porabi
	# (v procentih) preden ga mora ponovno zmesat
	pcent = 75.0
	
	# spremeni stanja gumbom
	bet_vnos.config(state = NORMAL)
	bet_bttn.config(state = NORMAL)
	
	# pocisti vse zadeve z mize (povrsine oz. Canvasa)
	povrsina.delete(ALL)
	# zbrisi ve karte iz vseh rok (moje, delivceve in moje odcepljene)
	mc = rm_array(mc)
	dc = rm_array(dc)
	mc1 = rm_array(mc1)
	
	# pogleda, ce mora ze zmesat kupcek
	# formula (za pcent = 75): porabljene*25 >= preostale*75
	# oz. porabljene : preostale = 75 : 25
	if float(len(used_c))*float(100-pcent) >= float(len(deck))*pcent:
		# vse vrednosti kart iz arraya porabljenih kart(used_c) 
		# pripni nazaj v kupcek (array  deck)
		for c in range(0,len(used_c)):
			card = used_c.pop(0)
			deck.append(card)
		# vse slike kart iz arraya porabljenih slik(used_s) 
		# pripni nazaj v array  slike
		for s in range(0,len(used_s)):
			slika = used_s.pop(0)
			slike.append(slika)
		
		# premesaj kupcek in o tem obvesti uporabnika
		deck, slike = same_random2(deck, slike)
		print "Zmesal kupcek!"
	
	# poglej ce je uporabnik spremenil hrbet karte
	#	in hrbet potem tudi ustrezno popravi
	back = bc.get()
	if back == "Roka":
		card_back = PhotoImage(file="slike/Deck1.gif")
	elif back == "Vrtnica":
		card_back = PhotoImage(file="slike/Deck2.gif")
	elif back == "Ovijalka":
		card_back = PhotoImage(file="slike/Deck3.gif")
	else:
		card_back = PhotoImage(file="slike/Deck2.gif")
	
	# razdeli karte za novo igro in zakrij prvo delivcevo karto
	for i in range (2):
		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(mc),180,image=slika)
		mc.append(card)
		
		back = povrsina.create_image(50 ,60,image=card_back)

		slika,card = dodaj(slike,deck)
		povrsina.create_image(calc_xcord(dc),60,image=slika)
		dc.append(card)
	# zakrij vse ostale karte
	back1 = povrsina.create_image(80 ,60,image=card_back)
	back2 = povrsina.create_image(50 ,180,image=card_back)
	back3 = povrsina.create_image(80 ,180,image=card_back)
	
	td = povrsina.create_text(50,120,text = "Vsota: X", fill="#FFFFFF")
	tm = povrsina.create_text(50,240,text = "Vsota: X", fill="#FFFFFF")

"""
Funkcija, ki pobrise vse elemente v arrayu
oz. ga unici in na novo naredi
@param
	a	=	array
@return
	a	=	array
"""
def rm_array(a):
	del a
	a = []
	return a

# funkcija, ki sprinta pravila BJ
def rules ():
	f = open("slike/pravila.txt", "r")
	for e in f:
		print e
	f.close()

"""
Funkcija, ki identicno premesa dva arraya 
(deluje samo, ce sta arraya enako dolga)
@param
	my_array1	=	array
	my_array2	=	array
@return
	my_subarray1	=	array; premesan my_array1
	my_subarray2	=	array; my_array2 premesan identicno kot my_array1
"""	
def same_random2(my_array1, my_array2):
	x_array = []
	my_subarray1 = []
	my_subarray2 = []
	
	# zmisli si n random cifer (cifra je lahko -1 ali 0), 
	# pri cemer je n dolzina arraya in jih pripni v x_array
	for i in range(len(my_array1)):
		x = randint(-1,0)
		x_array.append(x)
		
	# za vsako cifro e v x_arrayu iz obeh arrayev 
	# vzemi zadnji element in ju pripni na e-to mesto
	# v dveh novih arrayih (zato je random cifra tudi lahko samo 0 ali -1)
	for e in x_array:
		f = my_array1.pop(-1)
		my_subarray1.insert(e,f)
		g = my_array2.pop(-1)
		my_subarray2.insert(e,g)
	
	return my_subarray1, my_subarray2

"""
## Funkcija, ki jo poklice gumb Razcepi
		Razcep se lahko uporablja samo cisto na zacetku, ko imas se dve karti
		in imata le-ti enako vrednost. Z razcepom ju razdelis na dve loceni roki, 
		seveda moras tudi podvojiti stavo(se za drugo roko). To je pametna poteza
		ce dobis npr. dve osemki(16 je najslabsa mozna roka) ali ce dobis dva asa.
"""
def split():
	global back, bet, cash, id_, mc, mc1, possible, td, tm, tm1, used_s, vsota_, vsota_1
	# spremeni stanja gumbom
	split_bttn.config(state = DISABLED)
	double_bttn.config(state = DISABLED)
	surrender_bttn.config(state = DISABLED)
	
	# pocisti vse zadeve z mize (povrsine oz. Canvasa)
	povrsina.delete(ALL)
	# ko razcepis karte ne mores vec dobiti BJ
	possible = False
	# naredi se eno stavo za desno roko in popravi kolicino denarja
	bet = bet[0],bet[0]
	cash = cash-bet[0]
	money_v.set(str(cash))
	
	# ponovno narisi delivceve karte
	td = povrsina.create_text(50,120,text = "Vsota: X", fill="#FFFFFF")
	povrsina.create_image(50,60,image=used_s[-3])
	back = povrsina.create_image(50 ,60,image=card_back)
	povrsina.create_image(80,60,image=used_s[-1])
	# ponovno narisi moje karte...
	povrsina.create_image(50,180,image=used_s[-4])
	# eno premakni vstran in jo premakni tudi v nov array oz. novo roko
	povrsina.create_image(470,180,image=used_s[-2])
	mc1 = [mc.pop(-1)]
	# levi roki dodaj se eno karto
	slika,card = dodaj(slike,deck)
	povrsina.create_image(80,180,image=slika)
	mc.append(card)
	
	# poracunaj vrednosti kart in jih napisi
	vsota_ = suma(mc)
	tm = povrsina.create_text(50,240,text = "Vsota: " + vsota_[0]+str(vsota_[-1]), fill="#FFFFFF")
	vsota_1 = suma(mc1)
	tm1 = povrsina.create_text(470,240,text = "Vsota: " + vsota_1[0]+str(vsota_1[-1]), fill="#FFFFFF")
	
	# zdaj imas dve roki v igri
	id_ = 2

"""
## Funkcija, ki jo poklice gumb Stoj pa tudi se kaksna druga funkcija

// Pomembnejsi globalni spremenljivki sta:
		active	=	int; vrednost = 1 ali 2; ce si razcepil roko, pove katero 
						mora trenutno gledat - levo ali desno
		id_		=	int; vrednost = 1 ali 2; ce si razcepil roko zavzema vrednost 2, sicer 1
						(koliko rok imas v igri)
"""
def stand():
	global active, dc, deck, id_, mc, mc1, slike, td, tm1, vsota, vsota_1
	# uraden stoj -  s tem je delivec na vrsti
	if id_ == 1 or id_ ==2 and active == 2:
		# spremeni stanja gumbom
		hit_bttn.config(state = DISABLED)
		stand_bttn.config(state = DISABLED)
		double_bttn.config(state = DISABLED)
		surrender_bttn.config(state = DISABLED)
		split_bttn.config(state = DISABLED)
		
		# delivec je na vrst
		# poracunja vsoto, odkrij prvo karto in tolci dokler ne pride do 17 ali cez
		vsota = suma(dc)
		povrsina.delete(back)
		if vsota[-1] < 17:
			while vsota[-1] < 17:
				slika,card = dodaj(slike,deck)
				povrsina.create_image(calc_xcord(dc),60,image=slika)
				dc.append(card)
				vsota = suma(dc)
		# napisi vrednost delivcevih kart (poglej tudi za BJ)
		povrsina.delete(td)
		if blackjack(dc, False) == True:
			td = povrsina.create_text(50,120,text = "Blackjack", fill="#FFFFFF")
		else:
			td = povrsina.create_text(50,120,text = "Vsota: " + vsota[0]+str(vsota[-1]), fill="#FFFFFF")
		izid()
	
	# stoj, ki spremeni 'aktivno' roko
	# ce imas dve roki in je v drugi samo ena karta
	elif id_ == 2 and len(mc1) == 1:
		# aktivna roka je zdaj desna
		active = 2
		# razdeli ji se eno karto
		slika,card = dodaj(slike,deck)
		povrsina.create_image(500,180,image=slika)
		mc1.append(card)
		# poracunaj vsoto desne roke in jo napisi
		vsota_1 = suma(mc1)
		povrsina.delete(tm1)
		tm1 = povrsina.create_text(470,240,text = "Vsota: " + vsota_1[0]+str(vsota_1[-1]), fill="#FFFFFF")

"""
Funkcija, ki izracuna vrednost kart v roki
@param
	d	=	array; t.j. roka
@return
	b	=	double; vrednost roke, double je,ker je roka lahko tudi 'mehka', 
	ce imas npr. asa in 2 je to 'mehka 13', ce imas pa npr. asa, 2 in 10 
	je pa to samo 13; ce se as steje kot 11 je to mehka roka, 
	ce se steje kot 1 pa je to trda roka
"""
def suma(d):
	# stevec
	a = 0
	# koncni format
	b = "",0
	soft = True
	# pojdi po vseh kartah v roki; karte so oblike 'HT'(kar je srcna 10), 'D2' itd.
	for e in d:
		# dobi vrednost karte in jo pretvori v int
		a = e[-1]
		if a == "J" or a == "Q" or a == "K" or a == "T":
			a = 10
		elif a == "A":
			a = 11
		else:
			a = int(a)
		# sestej s prejsnjo vsoto
		b ="",a+b[-1]
	
	# poglej ce je treba asa obravnavat kot 1 in ne 11
	# pojdi po vseh kartah v roki...
	for f in d:
		# ce je vrednost kart nad 21 in je vmes kaksen as 
		if b[-1] > 21 and f[-1] == "A":
			# od vsote odstej 10(zdaj se as steje za 1 in ne vec 11)
			b ="", b[-1]-10
			# roka ni vec mehka
			soft = False
	# poglej ce je roka mehka
	# pojdi po vseh kartah v roki...
	for g in d:
		# ce je vmes kaksen as...
		if g[-1] == "A":
			# ce je roka se vedno lahko mehka...
			if soft == True:
				# napisi da je roka mehka
				b = "Mehka ", b[-1]
	return b

"""
## Funkcija, ki jo poklice gumb Predaja
		Predas se lahko samo na zacetku preden karkoli naredis(npr. da tolces)
		Uporablja se pri kartah, kjer je precej tveganja, da bos sel cez 21, ce bos tolkel,
		ampak je pa vrednost kart se pod 17 (npr. ce si iz prve dobil 16).
		S predajo dobis nazaj pol svoje stave
"""
def surrender():
	global bet, cash
	# vrni pol stave
	cash = cash+bet[0]/2
	money_v.set(str(cash))
	# napisi statistiko
	result_v.set("Zadnja igra: Predaja")
	bet_v.set("Zadnja stava: " + str(bet[0]))
	gain_v.set("Zadnji dobitek: " + str(bet[0]/2))
	cpub_v.set("Racunalnik busted? Ne" )
	# nova rnda
	new_round()

#------------------------------MENU--------------------------------#
menu = Menu(okno)
okno.config(menu=menu)
# FILE
filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Nova igra", command=lambda: new_game(nods))
filemenu.add_separator()
filemenu.add_command(label="Izhod", command=okno.quit)
# STEVILO KUPCKOV
deckmenu = Menu(menu)
menu.add_cascade(label="Stevilo kupckov", menu=deckmenu)
deckmenu.add_radiobutton(label="1 kupcek", command=lambda: new_game(1))
deckmenu.add_radiobutton(label="2 kupcka", command=lambda: new_game(2))
deckmenu.add_radiobutton(label="4 kupcki", command=lambda: new_game(4))
deckmenu.add_radiobutton(label="6 kupckov", command=lambda: new_game(6))
deckmenu.add_radiobutton(label="8 kupckov", command=lambda: new_game(8))
# POMOC
helpmenu = Menu(menu)
menu.add_cascade(label="Pomoc", menu=helpmenu)
helpmenu.add_command(label="Pravila", command=rules)
helpmenu.add_command(label="About...", command=about)
#--------------------------------------------------------------------#


#---------------------IGRALNA POVRSINA-----------------------#
povrsina = Canvas(okno, width=900, height=250, bg="#006500")
povrsina.grid(row=0, column=0,columnspan= 6)
#--------------------------------------------------------------------#


#----------------------------GUMBI--------------------------------#
# 1.STOLPEC
money = Label(okno, text="Denar: ")
money.grid(column=0, row=1)
bet_txt = Label(okno, text = "Stava")
bet_txt.grid(column=0, row=2)
# 2.STOLPEC
money_got = Label(okno, textvar = money_v)
money_got.grid(column=1, row=1, sticky=W)
bet_vnos = Entry(okno, width = 10, state = NORMAL)
bet_vnos.grid(column=1, row=2)
bet_bttn = Button(okno, text = "Stavi", width = 10, command = bet_f, state = NORMAL)
bet_bttn.grid(column=1, row=3)
# 3.STOLPEC
hit_bttn = Button(okno, text = "Tolci", width = 20, command = hit, state = DISABLED)
hit_bttn.grid(column=2, row=2)
stand_bttn = Button(okno, text = "Stoj", width = 20, command = stand, state = DISABLED)
stand_bttn.grid(column=2, row=3)
double_bttn = Button(okno, text = "Dvojna", width = 20, command = double, state = DISABLED)
double_bttn.grid(column=2, row=4)
split_bttn = Button(okno, text = "Razcepi", width = 20, command = split, state = DISABLED)
split_bttn.grid(column=2, row=5)
# 4.STOLPEC
surrender_bttn = Button(okno, text = "Predaja", width = 20, command = surrender, state = DISABLED)
surrender_bttn.grid(column=3, row=2)
nr_bttn = Button(okno, text = "Nov krog", width = 20, command = new_round)
nr_bttn.grid(column=3, row=3)
backc = OptionMenu(okno,bc,"Roka","Vrtnica","Ovijalka") 
backc.grid(column=3, row=4, sticky=NW)
#--------------------------------------------------------------------#

spacer=Label(okno)
spacer.grid(column=4, padx=190)

#--------------------------STATISTIKA----------------------------#
result= Label(okno, textvar = result_v)
result.grid(column=5, row=1, sticky=W)
bet_res = Label(okno, textvar = bet_v)
bet_res.grid(column=5, row=2, sticky=W)
gain = Label(okno, textvar = gain_v)
gain.grid(column=5, row=3, sticky=W)
cpu_b = Label(okno, textvar = cpub_v)
cpu_b.grid(column=5, row=4, sticky=W)
#--------------------------------------------------------------------#

spacer_=Label(okno)
spacer_.grid(column=6, padx=5)

# MAINLOOP
new_game()
okno.mainloop()

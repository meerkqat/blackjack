A Blackjack simulator written in Python 2.7 and Tkinter for a school project.
To run "Blackjack school issue" the "slike" directory is required to be in the 
same folder as the Python script. For the other two "pics" is instead required.

This was my first bigger project so it's kinda rough around the edges. 
The school issue and console only script were never updated, but I have updated 
Blackjack.py a bit since then.

The console only version is quite simplified. The only valid moves are "hit" 
(or "h") and "stop" (or "s").

The rules can be viewed in-game. They are printed to the console.
Blackjack Rules
Premise of the Game
The basic premise of the game is that you want to have a hand value that is closer to 21 than that of the dealer, without going over 21. The rules of play for the dealer are strictly dictated, leaving no decisions up to the dealer.  

Values of the Cards
In blackjack, the cards are valued as follows:
�	An Ace can count as either 1 or 11, as explained below.
�	The cards from 2 through 9 are valued at their face value.
�	The 10, Jack, Queen, and King are all valued at 10.
The suits of the cards do not have any meaning in the game. The value of a hand is simply the sum of the point counts of each card in the hand. For example, a hand containing (5,7,9) has the value of 21. The Ace can be counted as either 1 or 11. You need not specify which value the Ace has. It's assumed to always have the value that makes the best hand. An example will illustrate: Suppose that you have the beginning hand (Ace, 6). This hand can be either 7 or 17. If you stop there, it will be 17. Let's assume that you draw another card to the hand and now have (Ace, 6, 3). Your total hand is now 20, counting the Ace as 11. Let's backtrack and assume that you had instead drawn a third card which was an 8. The hand is now (Ace, 6, 8) which totals 15. Notice that now the Ace must be counted as only 1 to avoid going over 21. 
A hand that contains an Ace is called a "soft" total if the Ace can be counted as either 1 or 11 without the total going over 21. For example (Ace, 6) is a soft 17. The description stems from the fact that the player can always draw another card to a soft total with no danger of "busting" by going over 21. The hand (Ace,6,10) on the other hand is a "hard" 17, since now the Ace must be counted as only 1, again because counting it as 11 would make the hand go over 21. 

How the Dealer Plays His Hand
"Dealer stands on all 17s": In this case, the dealer must continue to take cards ("hit") until his total is 17 or greater. An Ace in the dealer's hand is always counted as 11 if possible without the dealer going over 21. For example, (Ace,8) would be 19 and the dealer would stop drawing cards ("stand"). Also, (Ace,6) is 17 and again the dealer will stand. (Ace,5) is only 16, so the dealer would hit. He will continue to draw cards until the hand's value is 17 or more. For example, (Ace,5,7) is only 13 so he hits again. (Ace,5,7,5) makes 18 so he would stop ("stand") at that point.
The dealer has no choices to make in the play of his hand. He must simply hit until he reaches at least 17 or busts by going over 21. 

What is a Blackjack, or a Natural?
A blackjack, or natural, is a total of 21 in your first two cards. A blackjack is therefore an Ace and any ten-valued card, with the additional requirement that these be your first two cards. If you split a pair of Aces for example, and then draw a ten-valued card on one of the Aces, this is not a blackjack, but rather a total of 21. The distinction is important, because a winning blackjack pays the player odds of 3 to 2. A bet of $10 wins $15 if the player makes a blackjack. A player blackjack beats any dealer total other than blackjack, including a dealer's three or more card 21. If both a player and the dealer have blackjack, the hand is a tie or push. 

The Player's Choices
Surrender
Surrender offers you as a player the choice to fold your hand, at the cost of half of the original bet. You must make that decision prior to taking any other action on the hand. For example, once you draw a third card, or split, or double down, surrender is no longer an option. 
Surrender is an excellent rule for players who use it wisely. Unfortunately, many players surrender far too many hands. If you play in a game with surrender, use our Blackjack Basic Strategy Engine to determine when surrender is the appropriate play. To understand how bad a hand must be to properly be surrendered, consider the following: To lose less with surrender, you must be only 25% likely to win the hand (ignoring pushes). That is, if you lose 75% of the time, and win only 25% of the time, your net loss is about 50% of your bets, equal to the amount you'll lose guaranteed by surrendering. So, learn to use the surrender option, but make sure you know when it is appropriate. 

Hitting/Standing
The most common decision a player must make during the game is whether to draw another card to the hand ("hit"), or stop at the current total ("stand").

Doubling Down
Among the more profitable player options available is the choice to "double down". This can only be done with a two card hand, before another card has been drawn. Doubling down allows you to double your bet and receive one, and only one, additional card to the hand. A good example of a doubling opportunity is when you hold a total of 11, say a (6,5) against a dealer's upcard of 5. In this case, you have a good chance of winning the hand by drawing one additional card, so you might as well increase your bet in this advantageous situation.

Splitting Pairs
When you are dealt a matching pair of cards (remember, ignore the suits), you have the ability to split the hand into two separate hands, and play them independently. Let's say you are dealt a pair of eights for a total of sixteen. Sixteen is the worst possible player hand, since it is unlikely to win as is, but is very likely to bust if you draw to it. Here's a great chance to improve a bad situation. 
And - always split a pair of Aces, but keep a pair of Jacks or other ten valued cards. 
